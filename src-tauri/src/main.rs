#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
#![feature(try_trait_v2)]

mod scoop_serdes;
use scoop_serdes::{scoop_search::ScoopSearchResult};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::{ops::{Try, FromResidual, ControlFlow}, convert::Infallible, fmt::{Display, Debug}};
use async_process::Command;

#[derive(Serialize, Debug, Clone)]
struct FailableScoopCommandResult<T> {
    on_success: Option<T>,
    on_error: Option<ScoopErrorString>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
struct ScoopErrorString(String);

impl<T> FailableScoopCommandResult<T> {
    const fn success(results: T) -> Self {
        Self {
            on_success: Some(results),
            on_error: None,
        }
    }

    const fn error(error: String) -> Self {
        Self {
            on_success: None,
            on_error: Some(ScoopErrorString(error)),
        }
    }
}

impl<T: Clone + Debug> Try for FailableScoopCommandResult<T> {
    type Output = T;
    type Residual = ScoopErrorString;

    fn from_output(output: Self::Output) -> Self {
        Self::success(output)
    }

    fn branch(self) -> std::ops::ControlFlow<Self::Residual, Self::Output> {
        let check = self.clone();
        match (check.on_success, check.on_error) {
            (Some(success), None) => ControlFlow::Continue(success),
            (None, Some(error)) => ControlFlow::Break(error),
            _ => ControlFlow::Break(ScoopErrorString(format!("Irrepresentable FailableScoopSearchResult: {:?}", self)))
        }
        
    }
}

impl<T> FromResidual<ScoopErrorString> for FailableScoopCommandResult<T> {
    fn from_residual(residual: ScoopErrorString) -> Self {
        Self::error(residual.0)
    }
}

impl<T, E: Display> FromResidual<Result<Infallible, E>> for FailableScoopCommandResult<T> {
    fn from_residual(residual: Result<Infallible, E>) -> Self {
        match residual {
            Err(err) => Self::error(err.to_string()),
            Ok(_) => panic!(),
        }
    }
}

fn main() {
    let t = tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![scoop_search, scoop_cat, download_package, scoop_info])
        .run(tauri::generate_context!());

    if let Err(e) = t {
        panic!("{:?}", e);
    }
}

#[tauri::command]
async fn scoop_search(search_term: &str) -> Result<FailableScoopCommandResult<Vec<ScoopSearchResult>>, ()> {
    Ok(_scoop_search(search_term).await)
}

async fn _scoop_search(search_term: &str) -> FailableScoopCommandResult<Vec<ScoopSearchResult>> {
    let output = Command::new("powershell")
        .arg("-Command")
        .arg(format!(
            "function global:Write-Host() {{}}; convertto-json @(scoop search {search_term})"
        ))
        .output()
        .await?;

    let stdout = output.stdout;
    let str = String::from_utf8(stdout)?;
    let packages = serde_json::from_str::<Vec<ScoopSearchResult>>(&str)?;
    FailableScoopCommandResult::success(packages)
}

#[tauri::command]
async fn scoop_cat(package_name: &str) -> Result<FailableScoopCommandResult<Value>, ()> {
    Ok(_scoop_cat(package_name).await)
}

async fn _scoop_cat(package_name: &str) -> FailableScoopCommandResult<Value> {
    let output = Command::new("powershell")
        .arg("-Command")
        .arg(format!("scoop cat {}", package_name))
        .output()
        .await?
        .stdout;

    let str = String::from_utf8(output)?;
    let cat_result = serde_json::from_str::<Value>(&str)?;
    FailableScoopCommandResult::success(cat_result)
}

#[tauri::command]
async fn download_package(package_name: &str) -> Result<(),()> {
    Command::new("powershell")
        .arg("-Command")
        .arg(format!("scoop install {}", package_name))
        .output().await;
    
    Ok(())
}

#[tauri::command]
async fn scoop_info(package_name: &str) -> Result<FailableScoopCommandResult<Value>, ()> {
    Ok(_scoop_info(package_name).await)
}

async fn _scoop_info(package_name: &str) -> FailableScoopCommandResult<Value> {
    let output = Command::new("powershell")
        .arg("-Command")
        .arg(format!("scoop info {} | convertto-json", package_name))
        .output()
        .await?
        .stdout;

    let str = String::from_utf8(output)?;
    let info_result = serde_json::from_str::<Value>(&str)?;
    FailableScoopCommandResult::success(info_result)
} 