use serde::{Serialize, Deserialize};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ScoopCatResult {
    pub version: String,
    pub description: String,
    pub homepage: String,
    pub license: String,
    pub notes: Vec<String>,
    pub architecture: Architecture,
    #[serde(rename = "extract_dir")]
    pub extract_dir: String,
    #[serde(rename = "post_install")]
    pub post_install: Vec<String>,
    pub bin: String,
    pub shortcuts: Vec<Vec<String>>,
    pub persist: Vec<String>,
    pub checkver: Checkver,
    pub autoupdate: Autoupdate,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Architecture {
    #[serde(rename = "64bit")]
    pub n64bit: n64bit,
    #[serde(rename = "32bit")]
    pub n32bit: n32bit,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct n64bit {
    pub url: String,
    pub hash: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct n32bit {
    pub url: String,
    pub hash: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Checkver {
    pub url: String,
    pub regex: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Autoupdate {
    pub architecture: Architecture2,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Architecture2 {
    #[serde(rename = "64bit")]
    pub n64bit: n64bit2,
    #[serde(rename = "32bit")]
    pub n32bit: n32bit2,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct n64bit2 {
    pub url: String,
    pub hash: Hash,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Hash {
    pub url: String,
    pub regex: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct n32bit2 {
    pub url: String,
    pub hash: Hash2,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Hash2 {
    pub url: String,
    pub regex: String,
}
