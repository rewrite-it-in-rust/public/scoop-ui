use serde::{Serialize, Deserialize};

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ScoopSearchResult {
    #[serde(rename(deserialize = "Name"))]  
    pub name: String,
    #[serde(rename(deserialize = "Version"))]  
    pub version: String,
    #[serde(rename(deserialize = "Source"))]  
    pub source: String,
    #[serde(rename(deserialize = "Binaries"))]  
    pub binaries: String,
}