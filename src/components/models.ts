export interface FailableScoopPackage<T> {
  on_success?: T,
  on_error?: OnErrorScoopPackage
}

export interface ScoopSearchResult {
  name: string
  version: string,
  source: string,
  binaries: string,
  additional_info: AdditionalInfo
}

export interface AdditionalInfo {
  cat?: FailableScoopPackage<ScoopCatResult>,
  info?: FailableScoopPackage<ScoopInfoResult>
}

export type OnErrorScoopPackage = string

export interface ScoopCatResult {
  version: string
  description: string
  homepage: string
  license: string
  notes: string[]
  architecture: Architecture
  extract_dir: string
  post_install: string[]
  bin: string
  shortcuts: string[][]
  persist: string[]
  checkver: Checkver
  autoupdate: Autoupdate
}

export interface Architecture {
  '64bit': N64bit
  '32bit': N32bit
}

export interface N64bit {
  url: string
  hash: string
}

export interface N32bit {
  url: string
  hash: string
}

export interface Checkver {
  url: string
  regex: string
}

export interface Autoupdate {
  architecture: Architecture2
}

export interface Architecture2 {
  '64bit': N64bit2
  '32bit': N32bit2
}

export interface N64bit2 {
  url: string
  hash: Hash
}

export interface Hash {
  url: string
  regex: string
}

export interface N32bit2 {
  url: string
  hash: Hash2
}

export interface Hash2 {
  url: string
  regex: string
}

export interface ScoopInfoResult {
  Name: string
  Description: string
  Version: string
  Bucket: string
  Website: string
  License: string
  'Updated at': UpdatedAt
  'Updated by': string
  Binaries: string
}

export interface UpdatedAt {
  value: string
  DisplayHint: number
  DateTime: string
}

